#ifndef __EstruturasDeDados__
#define __EstruturasDeDados__ 1

#define GRAU_ARVORE 2

typedef struct point 	{	int x;
							int y;
						} tPonto;
						
typedef struct quadrante { 	
					tPonto	pBase;
					int 	h,w;
					int 	nivel;
					int		cor;
					float	erro; //variancia
					// struct quadrante* childs;
					struct quadrante* childs;
				} 	tQuadrante;

/* IMPLEMENTACAO COM VETOR (MT DIFICIL) */
// typedef struct  {	tQuadrante* vet;
// 					int index;
// 					int max;
// 				} quadTree;

typedef struct {
	tQuadrante* root;
} quadTree;
				
								
#endif		// __EstruturasDeDados__

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include <GL/glut.h>

#include "EstruturasDeDados.h"
#include "winGL.h"
#include "arvore.h"
		
unsigned char* 	image = NULL;	// image file
int 			iHeight, 
				iWidth,
				iChannels;

bool 			desenha = false;

// ***********************************************
// ******                                   ******
// ***********************************************

int c1 = 64, c2 = 222, limit;

void printTree(tQuadrante* root){
	
	if(root->nivel == log2(iWidth) - 1){
		printf("node tal com cor %d no lll %d\n", (int)root->cor, root->nivel);
		return;
	}
	// printf("a\n");
	int i;
	for(i = 0; i < 4; i++)
		printTree(&(root->childs[i]));
	
	printf("node tal com cor %d no lll %d\n", (int)root->cor, root->nivel);
}

void desenhaArvore(tQuadrante* root) {
	if(root->nivel == limit){
		tPonto end;
		end.x = root->pBase.x + root->w;
		end.y = root->pBase.y + root->h;
		printf("%d\n", root->w);
		desenhaQuadrante(root->pBase, end, root->cor);
		return;
	}

	// printf("Aqui eu vou desenhar a arvore\n");
	
	// rotina que deve ser implementada para visualizacao da arvore
	// utilize a rotina desenhaQuadrante(p0, p1, cor)
	// fornecendo os pontos inicial e final do quadrante e a sua cor
	// funcao do valor do pixel ou da regiao que voce quer desenhar

	int i;
	for(i = 0; i < 4; i++)
		desenhaArvore(&(root->childs[i]));
}

unsigned int erroLimit = 0;

void desenhaArvoreByErro(tQuadrante* root) {
	if(root->erro <= erroLimit){
		tPonto end;
		end.x = root->pBase.x + root->w;
		end.y = root->pBase.y + root->h;
		printf("limit: %d\n", erroLimit);
		desenhaQuadrante(root->pBase, end, root->cor);
		return;
	}

	// printf("Aqui eu vou desenhar a arvore\n");
	
	// rotina que deve ser implementada para visualizacao da arvore
	// utilize a rotina desenhaQuadrante(p0, p1, cor)
	// fornecendo os pontos inicial e final do quadrante e a sua cor
	// funcao do valor do pixel ou da regiao que voce quer desenhar

	int i;
	for(i = 0; i < 4; i++)
		desenhaArvoreByErro(&(root->childs[i]));
}
	
/// ***********************************************************************
/// ** 
/// ***********************************************************************

// tPonto* pontoAlloc(int x, int y) {
// 	tPonto* ponto = malloc(sizeof(tPonto));

// 	if(ponto == NULL)
// 		return NULL;

// 	ponto->x = x;
// 	ponto->y = y;

// 	return ponto;
// }

// tQuadrante* quadranteAlloc(int x, int y, int h, int w, int nivel) {
// 	tQuadrante* quad = malloc(sizeof(tQuadrante));

// 	if(quad == NULL)
// 		return NULL;

// 	tPonto pBase;
// 	pBase.x = x;
// 	pBase.y = y;
// 	quad->pBase = pBase;
// 	quad->h = h;
// 	quad->w = w;
// 	quad->nivel = nivel;

// 	return quad;
// }

// void desc(tQuadrante* parent){
// 	int i;
// 	char tabs[] = "";
// 	for(i = 0; i < parent->nivel; i++){
// 		tabs[i] = '\t';
// 	}

// 	printf("%s nivel: %d\n",tabs, parent->nivel);
// 	printf("%s cor: %d\n",tabs, (int)parent->cor);
// 	printf("%s filhos: \n",tabs);
// 	desc(&(parent->childs[0]));
// 	desc(&(parent->childs[1]));
// 	desc(&(parent->childs[2]));
// 	desc(&(parent->childs[3]));

// }

void buildTree(tQuadrante* parent, int x, int y, int w, int h, int nivel){
	
}
// int kij = 0;

/* IMPLEMENTADO COM VETOR (MT DIFICIL)

int countParenstLevels(int size){
	int k = 0;
	while (size > 0){
		size /= 4;
		k += size;
	}

	return k;
}

bool initQuadTree(quadTree* tree){
	int iSize = iWidth * iHeight;
	int auxSize = countParenstLevels(iSize);

	tree->vet = malloc(sizeof(tQuadrante) * iSize + auxSize);
	if(tree->vet == NULL)
		return false;
	
	tree->index = 0;
	tree->max = iSize + auxSize;
	return true;
}

void buildQuadTree(quadTree* tree, int x, int y, int w, int h){
	// int i = tree->max - 1;
	int iSize = iWidth * iHeight;
	int i;
	for( i = 0 ; i < iSize; i++){
		tQuadrante aux;
		aux->cor = (int)image[iSize - 1 - i];
		aux.w = aux.h = 1;
		aux.erro = 0;
		aux.pBase
		// AAAAAAAHHHHHHHHHH TÁ TUDO ERRADO!!!!!!!!
		tree->vet[tree->max - 1 - i] = ;
	}
}
 */

tQuadrante* quadranteAlloc(int w, int h, int nivel, int N){
	tQuadrante* newQuadrante = malloc(sizeof(tQuadrante) * N);
	
	if(newQuadrante == NULL){
		return NULL;
	}

	int i;
	for(i = 0; i < N; i++){
		newQuadrante[i].w = w;
		newQuadrante[i].h = h;
		newQuadrante[i].nivel = nivel;
	}

	return newQuadrante;
}

void initQuadTree(tQuadrante** root){
	*root = quadranteAlloc(iWidth, iHeight, 0, 1);
	// (*root)->childs = malloc(sizeof(tQuadrante) * 4);
}

void makeChild(tQuadrante* root, tPonto p, int w, int h, int nivel){
	static int k = 0;
	// if(w == 1 && h == 1)
	root->pBase = p;
	if(w == 0){
		printf("pt [%03d][%03d]\n", p.x, p.y);
		root->cor = image[p.y * iWidth + p.x];
		root->erro = 0;
		return;
	}
	root->childs = quadranteAlloc(w/2, h/2, nivel + 1, 4);

	tPonto pts[4], aux = p;

	pts[0] = aux;
	aux.x += w/2;
	pts[1] = aux;
	aux.y += h/2;
	pts[2] = aux;
	aux.x -= w/2;
	pts[3] = aux;

	int i, colors = 0;
	float ccolors = 0, caux = 0;
	for(i = 0; i < 4; i++){
		makeChild(&(root->childs[i]), pts[i], w/2, h/2, nivel + 1);
		colors += root->childs[i].cor;
		if(root->childs[i].erro != 0){
			ccolors += root->childs[i].cor * 1.0/root->childs[i].erro;
			caux += 1.0/root->childs[i].erro;
		}else{
			ccolors += root->childs[i].cor;
			caux+= 1;
		}	
	}

	bool no_pond = true;

	if(no_pond)
		root->cor = colors/4;
	else
		root->cor = ccolors/caux;

	int erro = 0, delta;
	for(i = 0; i < 4; i++){
		delta = abs(root->childs[i].cor - root->cor);
		// delta = abs(root->childs[i].cor / (root->cor + 1) );
		// if(delta > erro)
		// 	erro = delta;
		erro += delta + root->childs[i].erro;
	}

	root->erro = erro / 4;// SHOWW!
	if(nivel == 8){
		k++;
	}
}

tQuadrante* imgTree;

void montaArvore() {
	
	printf("Aqui eu vou montar a arvore\n");

	initQuadTree(&imgTree);
	tPonto origin;
	origin.x = origin.y = 0;
	makeChild(imgTree, origin, iWidth, iHeight, 0);
	limit = log2(iWidth);
	
	// codifique aqui a sua rotina de montagem da arvore 
	
}
	
/// ***********************************************************************
/// ** 
/// ***********************************************************************

void teclado(unsigned char key, int x, int y) {

	switch (key) {
		case 27		: 	exit(0);
						break;				
		case 'q'	:
		case 'Q'	: 	montaArvore();
						break;				
		case 'i'	:
		case 'I'	: 	desenha = !desenha;
						break;
		case 'v'	:
		case 'V'	: 	desenha = !desenha;
						break;
		case '0'	: 	erroLimit= erroLimit == 0 ? 0 : erroLimit - 1;
						break;
		case '1'	: 	erroLimit++;
						break;
		}
	glutPostRedisplay();
}
		
/// ***********************************************************************
/// **
/// ***********************************************************************

void mouse(int button, int button_state, int x, int y ) {
	int max = (int)log2(iWidth);
	if 	(button_state == GLUT_DOWN ) {
		switch (button) {
			
			case GLUT_LEFT_BUTTON	: 	limit = limit == 0 ? max : limit - 1;
										erroLimit= erroLimit == 0 ? 0 : erroLimit - 1;
										break;
	
			case GLUT_RIGHT_BUTTON	:	limit = (limit)%max + 1;
										erroLimit++;
										break;
			}
		glutPostRedisplay();
		}
}

/// ***********************************************************************
/// ** 
/// ***********************************************************************

void desenho(void) {

    glClear (GL_COLOR_BUFFER_BIT); 
    
    glColor3f (1.0, 1.0, 1.0);
    
    if (desenha)
    	// desenhaArvore(imgTree);
		desenhaArvoreByErro(imgTree);
    else
    	glDrawPixels( iWidth, iHeight, GL_LUMINANCE, GL_UNSIGNED_BYTE, image );
   	
    glutSwapBuffers ();
}

/// ***********************************************************************
/// ** 
/// ***********************************************************************

int main(int argc, char** argv) {
	
char* filename = "images/lena.png";

    if (argc > 1)
		filename = argv[1];

	image = leImagem(filename);
			
	criaJanela(argc, argv);

    initOpenGL();
    
    initEventos();
    
    return 0;   
}
